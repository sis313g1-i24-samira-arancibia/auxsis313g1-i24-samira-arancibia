
# PRÁCTICA 4 💥
![im1](https://github.com/8victoria8/imagenes/assets/164128474/209af234-c496-4f1d-adab-6b18b63dfff2)

**SAMIRA VICTORIA ARANCIBIA VERA** 🐹

En una antigua profecía 📜, se predijo el nacimiento de un hombre con poderes🌀 de predicción llamado "Marketi Predictori". Años después, nace Lucas👦, un duende con habilidades para predecir el futuro. Sin embargo, no puede anticipar la llegada del demonio Valcorian😈, que lo secuestra para robar sus poderes. La hermana de Lucas, Rey👸, se embarca en una búsqueda para rescatarlo. Con la ayuda de aliados, utilizan un arma secreta, para derrotar a Valcorian y salvar a Lucas🌝.
---
 ### DIFICIL 🔥🔥🔥
![VILLANOS](https://github.com/8victoria8/imagenes/assets/164128474/b319b995-ebe9-45a7-b0e1-0fcd24264e63)
***
**NIVEL 1**

#### Empezaremos nuestra aventura moviendo al monstruos👽 a la tierra envenenada y a Rey👸 a la tierra saludable, y así con cada nivel siguiendo las reglas
***
**NIVEL 10**
#### En éste ejercicio debesmos ayudar a Rey👸 a lanzar el hechizo "¡Malago Borno!" usando grid-template-columns con los valores correctos. 
![NIVEL10](https://github.com/8victoria8/imagenes/assets/164128474/43aedfa6-8a57-460b-93f7-e53e1e92fb94)
***
**NIVEL 18**
#### En éste nivel debemos utilizar nuestro conocimiento😲 sobre grid-template-rows, grid-template-columns y gap y ayudar a Rey a lanzar el hechizo💥 "¡Awakozelo!" para derrotar a Azazel!😈
![NIVEL18](https://github.com/8victoria8/imagenes/assets/164128474/034c09bd-e66d-4ace-b34b-ba30e0ea8c93)
***
**NIVEL 20**
#### En éste nievel debemos combinar grid-column-start y grid-column-end para derrotar al monstruo!😈
![NIVEL20](https://github.com/8victoria8/imagenes/assets/164128474/738fbe2b-7990-4c78-a81b-ca554524c8fe)
***
**NIVEL 29**
#### Nos preparamos durante 29 niveles para en éste nivel poder enfrentarnos a Nightcall😈, ¡muestra a este demonio rojo de qué eres capaz con CSS grid!
![NIVEL29](https://github.com/8victoria8/imagenes/assets/164128474/63d19975-0e0e-4c91-91ee-588742d32f32)
***
**NIVEL 30**
#### Es nuestro turno de combinar grid-template-areas y grid-area para derrotar al monstruo💀
![NIVEL30](https://github.com/8victoria8/imagenes/assets/164128474/a8ff0a7e-32b3-4f94-9179-a76d45cf7205)
***
**NIVEL 40**
#### Intentan asustarnos😰 pero nos volvimos más fuertes , debemos usar grid-template-columns para que el navegador ajuste las columnas en la cuadrícula independientemente de su cantidad o tamaño de pantalla.
![NNIVEL40](https://github.com/8victoria8/imagenes/assets/164128474/02cc7b53-524e-4a5a-8e89-9a00be806d05)
***
**NIVEL 45**
####    Ahora nos enfrentamos a Evilins quienes son dos hermanos👬, uno de ellos le teme al agua🌊 (área azul). ¡Intenta usarla en su contra!
![NIVEL45](https://github.com/8victoria8/imagenes/assets/164128474/93ce1e36-f07e-45aa-905b-72ab04fba7b7)
***
**NIVEL 50**
### En éste nivel trabajamos con filas en una cuadrícula implícita para errotar al monstruo.👻
![NIVEL50](https://github.com/8victoria8/imagenes/assets/164128474/bca6fc89-0ced-4ec4-8558-a3e3f3dafb51)
***
**NIVEL 60**
#### Aprendemos una nueva propiedad de que establece tanto las propiedades align-items y justify-items en una sola declaración. Utilicemoslo para derrotar al monstruo💀
![NIVEL60](https://github.com/8victoria8/imagenes/assets/164128474/d206b916-300d-44c3-a533-d3bbc96d3349)
***
**NIVEL 66**
Ahora Rey👸se enfrenta al temible y arrogante Zarax y ebemos ayudarla a vencerlo.
![NIVEL66](https://github.com/8victoria8/imagenes/assets/164128474/bbc59d0c-d9c9-411f-bdc4-717d5385980c)
***
**NIVEL 70**
#### tenemos que combinar la alineación de la cuadrícula y la alineación de los elementos de la cuadrícula (justify-content y justify-items) para derrotar al monstruo😈
![NIVEL70](https://github.com/8victoria8/imagenes/assets/164128474/b8c5d79d-165f-4468-ac37-b87ead977df7)
***
**NIVEL 78**
#### En éste nivel nos encontramos con el gran Valcorian😮, el cuál duda de nuestras habilidades para derrotarlo, debemos demostrarle lo contrario😎
![NIVEL78](https://github.com/8victoria8/imagenes/assets/164128474/245c6248-9573-43b6-9b0b-418479e4282a)
***
**NIVEL 80**
#### Finalmente Rey👸encuentra a Lucas👦 su hermano, debemos usar la última propiedad de grid: grid para salvarlo✨
![NIVEL80](https://github.com/8victoria8/imagenes/assets/164128474/ae4b0a98-ac41-45fb-b14f-19f4fffab2bf)
***

## Los hermanos por fin están juntos
![hermanos](https://github.com/8victoria8/imagenes/assets/164128474/b43bc596-8e4c-4743-9c38-02320a97208e)

## Y así terminamos nuestra gran aventura...
![fin](https://github.com/8victoria8/imagenes/assets/164128474/9c32b09e-d436-48a6-a4b9-f2d4e873dc5a)

## 🔗 Links
[Enlace a Coding Fantasy](https://codingfantasy.com/)