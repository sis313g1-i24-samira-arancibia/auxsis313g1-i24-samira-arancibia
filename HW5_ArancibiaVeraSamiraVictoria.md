
***
# 🍱**Práctica 5**🍱

```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
|     🍄Univ:Samira Victoria Arancibia Vera🍄        |
|     💻SIS-313: Diseño y Programación Gráfica💻     |
|     👨‍💻Docente: Ing. Jose David Mamani Figueroa👨‍💻   |
|    🐱Aux: Univ. Sergio Moises Apaza Caballero🐱    |
|                  🎓U.A.T.F.🎓                      |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||

```
***
## 🍽️🍎🍊🥒 **CSS DINNER** 🥒🍊🍎🍽️

![decor1](https://github.com/8victoria8/imagenes/assets/164128474/9af2d11c-6f46-4bea-afa5-090607db2cd7)
***

### 🎈¡Ven y Únete a la Fiesta de la Comida más Increíble! 🎈

😃Imagina que estás en una gran fiesta🎉 de comida, pero todas las comidas están desordenadas🎇 y necesitan ser ordenadas correctamente en la mesa🍱. En el juego CSS Dinner, tú eres el chef👨‍🍳 y tienes que usar tus habilidades para organizar la comida de manera adecuada. 🥒🍊🍎🍱
***
⚠️
- [X]  COMPLETA LOS 32 NIVELES Y SUBE LAS CAPTURAS DE LOS PRIMEROS 5 Y LOS ÚLTIMOS 10 NIVELES
***
- ###  **NIVEL 1** 🍽️🍽️
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
| En éste nivel debemos seleccionar elementos según su|
| tipo.Selecciona todos los elementos del tipo A.     |
| El tipo se refiere al tipo de etiqueta, por lo que  |
| <div>, <p> y <ul> son todos tipos de elementos      |
| diferentes.                                         |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
- Solución🍭🌷🍶
```css
plate
```
![1 A](https://github.com/8victoria8/imagenes/assets/164128474/d5d481fa-d8ea-4496-8c8c-daeb66e57b4d)
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **NIVEL 2** 🍱🍽️🍱
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
| En éste nivel debemos seleccionar elementos según su|
| tipo.Selecciona todos los elementos del tipo A.     |
| El tipo se refiere al tipo de etiqueta, por lo que  |
| <div>, <p> y <ul> son todos tipos de elementos      |
| diferentes.                                         |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
- Solución🍭🌷🍶
```css
bento
```
![2 A](https://github.com/8victoria8/imagenes/assets/164128474/fc7ec802-fc1b-4e9d-a33a-e43e383807fd)
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **NIVEL 3** 🍽️🍽️🍱
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
| Selecciona el elemento con un id específico.También |
| puedes combinar el selector de id con el selector de|
| tipo.                                               |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
- Solución🍭🌷🍶
```css
#fancy
```
![3  #id](https://github.com/8victoria8/imagenes/assets/164128474/238d16ba-5c69-4d82-915d-331fa5e8be5f)
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **NIVEL 4** 🍱🍎🍎
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
| Selecciona todos los elementos B dentro de A.B se   |
| llama descendiente porque está dentro de de otro    |
| elemento.                                           |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
- Solución🍭🌷🍶
```css
plate apple
```
![4  A B](https://github.com/8victoria8/imagenes/assets/164128474/e1dd25d8-9225-4090-a677-0d4ea33061d0)
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **NIVEL 5** 🍽️🍊🥒🥒🍽️
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
|Puedes combinar cualquier selector con el descendente|
| elegido.                                            |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
- Solución🍭🌷🍶
```css
#fancy pickle
```
![5  #id A](https://github.com/8victoria8/imagenes/assets/164128474/8390fd08-0058-4d00-a512-ead886087c9f)
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **NIVEL 23** 🍽️🍎🍎🥒🍽️
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
| Selecciona el único elemento de su tipo dentro de   |
| elemento.                                           |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
- Solución🍭🌷🍶
```css
apple:only-of-type
```
![23  only-of-type](https://github.com/8victoria8/imagenes/assets/164128474/5e8ba766-cad8-468a-97ff-1c955055b773)
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **NIVEL 24** 🍽️🍊🍊🥒🥒🍎🍎🍽️
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
| Selecciona cada último elemento de ese tipo dentro  |
| de otro elemento. Recuerda que el tipo se refiere   |
| al tipo de etiqueta, por lo que <p> y <span> son    |
| tipos diferentes.                                   |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
- Solución🍭🌷🍶
```css
.small:last-of-type
```
![24  last-of-child](https://github.com/8victoria8/imagenes/assets/164128474/ab0a17a4-ebb8-42dd-88db-2b5348ab3186)
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **NIVEL 25** 🍱🥒🍽️🍱
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
| Selecciona elementos que no tienen ningún otro      |
| elemento dentro de ellos.                           |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
- Solución🍭🌷🍶
```css
bento:empty
```
![25 empty](https://github.com/8victoria8/imagenes/assets/164128474/98dcd240-795c-4d8a-8d7e-e8c60b2301fe)
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **NIVEL 26** 🍽️🍎🍎🍎🍊🥒🍽️
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
| Selecciona todos los elementos que no coinciden con |
| el selector de negación :not(X). Puedes usar esto   |
|para seleccionar todos los elementos que no coinciden|
| con el selector (X).                                |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
- Solución🍭🌷🍶
```css
apple:not(.small)
```
![26  not( small)](https://github.com/8victoria8/imagenes/assets/164128474/e5ef4214-5271-4e57-ad18-247da786cc21)
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **NIVEL 27** 🍽️🍎🍎🥒🍊🥒🍽️
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
|Selecciona todos los elementos que tienen un atributo|
|específico [attribute]. Los atributos aparecen dentro|
|de la etiqueta de apertura de un elemento, como esto:|
|<span attribute="value">.Un atributo no siempre tiene|
|un valor, ¡puede estar en blanco!                    |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
- Solución🍭🌷🍶
```css
[for]
```
![27 for](https://github.com/8victoria8/imagenes/assets/164128474/5e39bd76-f27a-45e4-bf3c-c9603c306322)
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **NIVEL 28** 🍽️🥒🍎🍽️🍱
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
| Selecciona todos los elementos A que tengan un      |
| atributo específico. Puedes combinar el selector de |
|atributo con otro selector(como el selector de nombre|
|de etiqueta) agregándolo al final.                   |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
- Solución🍭🌷🍶
```css
plate[for]
```
![28  plate for](https://github.com/8victoria8/imagenes/assets/164128474/ee08c1e7-2467-4894-a600-75269b1c1e3b)
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **NIVEL 29** 🍱🍎🍎🍊🥒🍱
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
|Selecciona todos los elementos que tengan un valor de|
|atributo específico [attribute="value"]. Los         |
|selectores de atributos son sensibles a mayúsculas y |
|minúsculas, cada carácter debe coincidir exactamente.|
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
- Solución🍭🌷🍶
```css
[for="Vitaly"]
```
![29   for Vitaly](https://github.com/8victoria8/imagenes/assets/164128474/c9ed1f84-548d-4d1a-ae1a-f0fd9761a8f5)
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **NIVEL 30** 🍽️🥒🍎🍊🍱
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
|Selecciona todos los elementos con un valor de       |
|atributo que comience con caracteres específicos     |
|[attribute^="value"].                                |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
- Solución🍭🌷🍶
```css
[FOR^="Sa"]
```
![30  FOR](https://github.com/8victoria8/imagenes/assets/164128474/2cf20ec6-8fb1-47d1-94d9-2e771bd3533f)
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **NIVEL 31** 🍎🥒🍎🍊🥒
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
|Selecciona todos los elementos con un valor de       |
|atributo que comience con caracteres específicos     |
| [attribute$="value"].                               |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
- Solución🍭🌷🍶
```css
[for$="ato"]
```
![31 for](https://github.com/8victoria8/imagenes/assets/164128474/6db89bec-d81f-4ecc-91c4-311673a35154)
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **NIVEL 32** 🍱🍎🥒🍊🍱
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
|Selecciona todos los elementos que tengan un valor de|
|atributo  que contenga caracteres específicos en     |
|cualquier parte [attribute*="value"]. Es un selector |
|útil si puedes identificar un patrón común en cosas  |
|como atributos de clase, href o src.                 |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
- Solución🍭🌷🍶
```css
[for*="obb"]
```
![32 final](https://github.com/8victoria8/imagenes/assets/164128474/2ff97620-7c71-43a4-b9e9-ac497cc8c831)
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
![deco2](https://github.com/8victoria8/imagenes/assets/164128474/7f64751b-802d-4331-8939-e44921f6af1c)
***
## 🍽️🏆👨‍🍳**FIN**👨‍🍳🏆🍽️
😎Logramos superar con éxito cada uno de los niveles😎
![deco4](https://github.com/8victoria8/imagenes/assets/164128474/44c42bab-5d74-4058-a8c6-6fbf8302b483)
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
## 🔗 Enlaces Útiles
- [Código de CSS DINNER](https://flukeout.github.io/)
