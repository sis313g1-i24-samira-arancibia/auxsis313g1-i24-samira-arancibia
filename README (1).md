
# SAMIRA ARANCIBIA

Por fin presento mi tarea.
En todas estas imagenes veremos la forma de derrotar a todos los villanos de nuestra historia, empezando a combatir con criaturas poco peligrosas hasta llegar al gefe final.

![GATITO](https://github.com/8victoria8/imagenes/assets/164128474/0721796b-fff6-4831-91dd-b26275266eaf)

***MEDIUM***

![LEVEL21-MEDIUM](https://github.com/8victoria8/imagenes/assets/164128474/0d5aa0dc-cc87-4a4f-9843-77dd63dec325)

![LEVEL22-MEIUM](https://github.com/8victoria8/imagenes/assets/164128474/6032e921-76d3-4d46-a1ef-32930b0de4d1)

![LEVEL23-MEDIUM](https://github.com/8victoria8/imagenes/assets/164128474/5cf3510e-8594-45d5-9db4-3a834883da05)

![LEVEL24-MEDIUM](https://github.com/8victoria8/imagenes/assets/164128474/b4c3c2f4-a4d9-4189-ba0b-2da2bd3fa249)

***HARD***
![GATITO2](https://github.com/8victoria8/imagenes/assets/164128474/7e519d75-3858-425e-b30e-e2e852b3e847)

![LEVEL21-HARD](https://github.com/8victoria8/imagenes/assets/164128474/f553c3d5-9b07-481c-8773-32b90900b85f)

![LEVEL22-HARD](https://github.com/8victoria8/imagenes/assets/164128474/849c58d2-f194-4ed0-b533-dda2ea4fbac8)

![LEVEL23-HARD](https://github.com/8victoria8/imagenes/assets/164128474/82a52cb3-d698-4091-acc0-279d3196dfc0)

![LEVEL24-HARD](https://github.com/8victoria8/imagenes/assets/164128474/76bb40d1-aaf4-4ff1-8cfc-394f3adae85e)

Y terminamos nuestra maravillosa aventura...
O quizás no.
![gato3](https://github.com/8victoria8/imagenes/assets/164128474/c0e1b4ae-8131-45d4-96a5-4ed2a67b5c2a)
