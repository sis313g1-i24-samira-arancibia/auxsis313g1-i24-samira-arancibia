
***
#  🍄 **Práctica 8**  🍄 

```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
|     🍄Univ:Samira Victoria Arancibia Vera🍄        |
|     💻SIS-313: Diseño y Programación Gráfica💻     |
|     👨‍💻Docente: Ing. Jose David Mamani Figueroa👨‍💻   |
|    🐱Aux: Univ. Sergio Moises Apaza Caballero🐱    |
|                  🎓U.A.T.F.🎓                      |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||

```
***
- ###  **PREGUNTA 1** 🍄 🌻 🍄
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
| Adjunte capturas de pantalla mostrando la versión   |
| de node y de npm mediante comandos por la terminal  |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
***
![ABCDE](https://github.com/8victoria8/AGGGG/assets/164128474/d4553071-4bb3-4e16-9729-8ada4a50ad80)
***
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
***
- ###  **PREGUNTA 2** 🍄 🌻 🍄
***
   🌸[Video](https://drive.google.com/drive/folders/1o0cwXw6ze8tv_-SjkTLVldS82ttQXiGB?usp=sharing)🌸
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
| En el video explicamos:                             |
| -La creación de una carpeta en el disco local C:,   |
| mediante comandos desde laterminal.                 |
| -La creación de un proyecto utilizando React con    |
|  Next.js .                                          |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```

***
- ###  **PREGUNTA 3** 🍄 🌻 🍄
***
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
|  Utilizamos los siguientes comandos:                |
|  -cd ..                                             |
|  -code                                              |
|  -npm run dev                                       |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **PREGUNTA 4** 🍄 🌻 🍄
***
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
| Durante el laboratorio no utilice el comando cd ..  |
| para salir al disco local c.                        |
| Mi intenet estaba lento por lo que tuve que usar    |
| "install".                                          |
| Olvide usar npm run dev al hacer la tarea.          |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||

```
***
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **PREGUNTA 5** 🍄 🌻 🍄
***
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
| Eso sucede cuando clonamos en repositorio, cuando   |
| existe esa carpeta dentro del repositorio principal |
| Mi intenet estaba lento por lo que tuve que usar    |
| "install". Para solucionarlo borrramos la carpeta,  |
| no la principal, y volver a subir el repositorio    |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||

```
***
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
