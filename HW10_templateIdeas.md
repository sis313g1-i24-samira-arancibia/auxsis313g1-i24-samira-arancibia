***
# 🌼**Práctica 10**🌼

```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
|     🍄Univ:Samira Victoria Arancibia Vera🍄        |
|     💻SIS-313: Diseño y Programación Gráfica💻     |
|     👨‍💻Docente: Ing. Jose David Mamani Figueroa👨‍💻   |
|    🐱Aux: Univ. Sergio Moises Apaza Caballero🐱    |
|                  🎓U.A.T.F.🎓                      |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||

```
***
```bash
🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀
🍀            🍄LINK DE LOS TEMPLATES🍄            🍀
🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀
```
🌱 [TEMPLATE1](https://freebiesbug.com/figma-freebies/plant-shop-template/) 🌱
***
💜 [TEMPLATE2](https://freebiesbug.com/figma-freebies/hydra/) 💜
***
```bash
🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀
🍀                🍄LINK EN FIGMA🍄                🍀
🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀
```
🌱 [FIGMA1](https://www.figma.com/file/cQRjY9XJEm19KbFM0XBy2K/Interactive-Portfolio-Website%3A-Figma-UI-Design-Tutorial-for-Beginners-%7C-Step-by-Step-Guide-(Community)?type=design&node-id=0-1&mode=design&t=v6YAuZWlR0A6P56r-0) 🌱
***
💜 [FIGMA2](https://www.figma.com/file/l2i1YByRKZHc2X67Xyklzp/Hydra-Landing-Page-(Community)?type=design&node-id=0-1&mode=design&t=LZ7qNgxFXJTtr7Mq-0) 💜
***
```bash
🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀
🍀         🍄LINK DEl NUEVO REPOSITORIO🍄          🍀
🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀
```
***
🌕 [REPOSITORIO NUEVO](https://gitlab.com/samiraarancibiavera/react-with-samiraaarancibia.git) 🌕
***
🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀🍀
