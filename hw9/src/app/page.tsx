import Image from "next/image";
import styles from "./page.module.css";
import Button from "@/components/Button";
import Pokemon from "@/components/Pokemon";
import Video from "@/components/Video";
import { sources } from "next/dist/compiled/webpack/webpack";
export default function Home() {
  return (
    <main>
      <div>
      <Button title="HOLA"></Button>
      <Button title="ADIOS" transparent={true}></Button>
      </div>
      <div className="container-poki">
        <Pokemon name= "LEAFEON" imgurl="./images/leafeon.jpg" type="Planta"></Pokemon>
      </div>
      <div className="container-video">
        <div className="vidd v1">
          <Video title="Young and Beautiful" artist="Lana del Rey" videourl="/videos/YOUNG.mp4"></Video>
        </div>
        <div className="vidd v2">
          <Video title="Hysteria" artist="Muse" videourl="/videos/HYSTERIA.mp4"></Video>
        </div>
        <div className="vidd v3">
          <Video title="I Wanna Be Your Slave" artist="Maneskin" videourl="/videos/I WANNA BE YOUR SLAVE.mp4"></Video>
        </div>
      </div>
    </main> 
  )
}
