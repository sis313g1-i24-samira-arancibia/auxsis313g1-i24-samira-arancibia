export default function Pokemon(props:{name:string,imgurl:any,type:string}){
    return(
        <div className="poki">
            <h1>{props.name}</h1>
            <img className="poki-img" src={props.imgurl} alt="" />
            <p>{props.type}</p>
        </div>

    )
}