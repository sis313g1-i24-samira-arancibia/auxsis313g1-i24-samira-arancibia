export default function Button(props:{title:string, transparent?:boolean}){
    return(
        props.transparent?
        <button className="btn btn-transparent">{props.title}</button>
        :
        <button className="btn btn-primary">{props.title}</button>
        
    )
} 