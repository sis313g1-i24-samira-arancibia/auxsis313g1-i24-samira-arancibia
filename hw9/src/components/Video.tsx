export default function Video(props:{title:string, artist:string, videourl:any}){
    return(
        <div className="ori">
        <h2 className="ori-title">{props.title}</h2>
        <p className="ori-artist">{props.artist}</p>
        <video className="ori-vid" src={props.videourl}controls> </video>
        </div>
    )
}