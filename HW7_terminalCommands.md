
***
#  🍄 **Práctica 7**  🍄 

```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
|     🍄Univ:Samira Victoria Arancibia Vera🍄        |
|     💻SIS-313: Diseño y Programación Gráfica💻     |
|     👨‍💻Docente: Ing. Jose David Mamani Figueroa👨‍💻   |
|    🐱Aux: Univ. Sergio Moises Apaza Caballero🐱    |
|                  🎓U.A.T.F.🎓                      |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||

```
***
- ###  **PREGUNTA 1** 🍄 🌻 🍄
***
   🌸[Video](https://drive.google.com/drive/folders/1VKL8PWhsk5zlSb4eT9gz6hEion4gqFZR?usp=drive_link)🌸
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
| En el video explicamos:                             |
| -La creación de una carpeta en el disco local C:,   |
| mediante comandos desde laterminal.                 |
| -La forma de cambiar de directorios utilizando      |
| comandos.                                           |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **PREGUNTA 2** 🍄 🌻 🍄
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
| Realizamos el mismo proceso del video 3 veces más.  |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
***
![ABC](https://github.com/8victoria8/AGGGG/assets/164128474/f221d11c-09fd-4217-a237-958935c69c6a)
***
![ABC2](https://github.com/8victoria8/AGGGG/assets/164128474/b0b635b5-49fe-414d-97cf-4c3e21e5ac78)
***
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
|    En las imágenes podemos ver la creacion de 3     |
|   carpetas dentro del disco local C y también la    |
|          manera de cambiar los directorios.         |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **PREGUNTA 3** 🍄 🌻 🍄
***
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
|  Utilizamos los siguientes comandos:                |
|  -cd ..                                             |
|  -cd                                                |
|  -md                                                |
|  -dir                                               |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
```
```bash
🍄|||||||||||||||||||||||||🍄||||||||||||||||||||||||🍄

```
***
- ###  **PREGUNTA 4** 🍄 🌻 🍄
***
```bash
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
| Se utilizó los siguientes comandos.                 |
| cd .. [para poder salir del directorio]             |
| cd carpeta1 [para entrar a un directorio]           |
| md carpeta1  [para crear la carpeta]                |
| dir   [para ver todos los archivos que tenemos]     |
| cls [para limpiar la terminal]                      |
|||||||||||||||||||||||||||||||||||||||||||||||||||||||

```
